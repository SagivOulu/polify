package physics;

import geometry.*;
import matrices.*;
import polify.*;

public class CollisionData {
	private final float COR;
	private final Impulse[] impulses;
	private final Vector2d contact, normal;
	
	public static CollisionData getCollisionData(BodyPair bodyPair, IntersectionData intersectionData, float COR) throws NullPointerException {
		if (bodyPair == null)
			throw new NullPointerException("cannot create collision data using a null body pair");
		if (intersectionData == null) 
			throw new NullPointerException("cannot create collision data using a null intersection data");
		
		return new CollisionData(bodyPair, intersectionData.getClip().getCentroid(), intersectionData.getMTV(), COR);
	}
	
	private CollisionData(BodyPair bodyPair, Vector2d contact, Vector2d normal, float COR) throws NullPointerException {
		if (bodyPair == null)
			throw new NullPointerException("cannot create collision data using a null body pair");
		if (contact == null)
			throw new NullPointerException("cannot create collision data using a null contact Vector2d");
		if (normal == null)
			throw new NullPointerException("cannot create collision data using a null normal Vector2d");
		this.COR = COR;
		this.normal = normal;
		this.contact = contact;
		this.impulses = this.calcImpulses(bodyPair);
	}
	
	private Impulse[] calcImpulses(BodyPair bodyPair) {
		assert(this.normal != null && this.contact != null);
		Body A = bodyPair.getBodyA(), 
		B = bodyPair.getBodyB();
		//Polygon AP = A.getWorldSpacePolygon();
		
		//get relative speed
		Vector2d relativeSpeed = A.getVelocityAt(getContact())//A.getVelocityAt(A.toBodySpacePoint(getContact()))
				.sub(B.getVelocityAt(getContact()));//.sub(B.getVelocityAt(B.toBodySpacePoint(getContact())));
		//get normal of contact edge pointing out from the polygon
		Vector2d normal = getNormal();//AP.getEdge(getContact().getEdgeIndex()).getVector().rotate((float)Math.PI / 2);
		
		//Vector2d nextEdge = AP.getEdge((getContact().getEdgeIndex() + 1) % AP.degree()).getVector();
		//if (normal.dot(nextEdge) > 0)
		//	normal = normal.mul(-1);
		
		//check if colliding or detaching
		if (relativeSpeed.dot(normal) <= 0) return null;//if not moving towards each other
		
		//j = [j calculation...]
		Vector2d RapT = (new Vector2d(A.getCM(), getContact())).rotate((float)Math.PI / 2);
		Vector2d RbpT = (new Vector2d(B.getCM(), getContact())).rotate((float)Math.PI / 2);
		float aPow = (float)Math.pow(RapT.dot(normal), 2);
		float bPow = (float)Math.pow(RbpT.dot(normal), 2);
		float j = relativeSpeed.mul(-1 - COR).dot(normal) / 
			(normal.dot(normal) * ( A.getInvMass() + B.getInvMass() ) + aPow * A.getInvInertia() + bPow * B.getInvInertia() );
		
		Impulse[] impulses = new Impulse[2];
		
		impulses[0] = new Impulse(
				normal.mul(+j),
				+1 * RapT.dot(normal.mul(j)));
		
		impulses[1] = new Impulse(
				normal.mul(-j),
				-1 * RbpT.dot(normal.mul(j)));
		
		return impulses;
	}
	
	public float getCOR() { return this.COR; }
	
	public Vector2d getContact() {
		assert(this.contact != null);
		return this.contact;
	}
	
	public Vector2d getNormal() {
		assert(this.normal != null);
		return this.normal;
	}
	
	public Impulse[] getImpulses() {
		assert(this.impulses == null || 
				(this.impulses.length == 2 && this.impulses[0] != null && this.impulses[1] != null));
		return this.impulses;
	}
	
	public void simulate(BodyPair bodyPair) throws NullPointerException {
		if (bodyPair == null)
			throw new NullPointerException("cannot simulate a collision using a null body pair");
		assert(getContact() != null && getNormal() != null && getImpulses() != null);
		Impulse[] impulses = getImpulses();
		if (impulses == null) return;
		assert(impulses.length == 2 && impulses[0] != null && impulses[1] != null);
		bodyPair.getBodyA().applyWorldSpaceImpulse(impulses[0]);
		bodyPair.getBodyB().applyWorldSpaceImpulse(impulses[1]);
	}
}
