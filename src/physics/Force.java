package physics;

import matrices.*;
public class Force extends Vector2d{
	private Vector2d origin;
	
	public Force(Vector2d fv, Vector2d origin) throws NullPointerException {
		super(fv);
		origin(origin);
	}
	
	public void origin(Vector2d origin) { this.origin = origin; }
	
	public Vector2d origin() { return this.origin; }
}
