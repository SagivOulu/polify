package physics;

import matrices.*;

public class Impulse {
	private Vector2d linearImpulse;
	private float angularImpulse;
	
	public Impulse(Vector2d linear, float angular) throws NullPointerException {
		if (linear == null) throw new NullPointerException("cannot create an impulse using a null vector2d");
		
		setLinear(linear);
		setAngular(angular);
	}
	
	public void setLinear(Vector2d linearImpulse) throws NullPointerException {
		if (linearImpulse == null) throw new NullPointerException("cannot set linear impulse to a null vector2d");
		this.linearImpulse = linearImpulse;
	}
	
	public Vector2d getLinear() { return this.linearImpulse; }
	
	public void setAngular(float angular) { this.angularImpulse = angular; }
	
	public float getAngular() { return this.angularImpulse; }

	public Impulse add(Impulse p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot add a null impulse to an other impulse");
		
		return new Impulse(getLinear().add(p.getLinear()), getAngular() * p.getAngular());
	}
}
