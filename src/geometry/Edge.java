package geometry;

import matrices.*;

public class Edge {
	private Vector2d start, end;
	
	public Edge(Vector2d start, Vector2d end) throws NullPointerException, RuntimeException{
		if (start == null || end == null)
			throw new NullPointerException("cannot create edge using null points");
		if (start.isEqual(end))
			throw new RuntimeException("cannot create an edge using two identical points");
		this.start = start;
		this.end = end;
	}
	
	public void setStart(Vector2d start) throws NullPointerException, RuntimeException {
		if (start == null) throw new NullPointerException("cannot set edge start point to a null vector");
		if (start.isEqual(getEnd()))
			throw new RuntimeException("cannot set edge start to a point equal to edge end");
		this.start = start;
	}
	
	public void setEnd(Vector2d end) throws NullPointerException, RuntimeException {
		if (start == null) throw new NullPointerException("cannot set edge end point to a null vector");
		if (end.isEqual(getStart()))
			throw new RuntimeException("cannot set edge end to a point equal to edge start");
		this.end = end;
	}
	
	public Vector2d getStart() { return this.start; }
	
	public Vector2d getEnd() { return this.end; }

	public Vector2d getVector() { return new Vector2d(getStart(), getEnd()); }
	
	public boolean isOnLeft(Vector2d p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot check if a null point is left to an edge");
		return getVector().cross2d(p.sub(getStart())) < 0;
	}
	
	public boolean isOnRight(Vector2d p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot check if a null point is right to an edge");
		return getVector().cross2d(p.sub(getStart())) > 0;
	}
	
	public Vector2d computeIntersection(Edge e) throws NullPointerException, RuntimeException {
		if (e == null)
			throw new NullPointerException("cannot compute intersection with a null edge");
		if (this.getVector().cross2d(e.getVector()) == 0)
			throw new RuntimeException("cannot compute intersection of parallel edges");
		float b = (this.getStart().sub(e.getStart())).cross2d(this.getVector()) / 
				e.getVector().cross2d(this.getVector());
		Vector2d intersection = e.getStart().add(e.getVector().mul(b));
		return intersection;
	}
}
