package geometry;

import matrices.*;

/**
 * a vector based polygon.
 * @author sagiv
 *
 */
public class Polygon {
	private Vector2d[] vertices;
	private BoundingRectangle boundingRectangle = null;
	private boolean boundingRectangleDirty = true;
	private Vector2d centroid = null; 
	
	/**
	 * a constructor of a polygon from an array of vertices.
	 * note - the order of the vertices matters. out of order vertices array may result in a self intersecting polygon
	 * @param vertices an array of Vector2ds, representing the vertices of the polygon
	 * @throws NullPointerException if the array is null, or one of its cells
	 */
	public Polygon(Vector2d[] vertices) throws NullPointerException, RuntimeException {
		if (vertices == null) throw new NullPointerException("cannot create polygon with a null array of vertices");
		if (vertices.length < 3) throw new RuntimeException("a polygon must have 3 vertices or more: " + vertices.length);
		this.vertices = new Vector2d[vertices.length];
		if (vertices[0] == null || vertices[1] == null || vertices[2] == null)
			throw new NullPointerException("cannot create polygon with one null vertex or more");
		
		boolean switchOrder = new Edge(vertices[0], vertices[1]).isOnRight(vertices[2]); 
		for (int i = 0; i < this.degree(); i++) {
			if (vertices[i] == null) throw new NullPointerException("cannot create polygon with one null vertex or more");
			if (switchOrder) 	this.setVectex(this.degree() - 1 - i, vertices[i]);
			else 				this.setVectex(i, vertices[i]); 
		}
		
		//calcBoundingRectangle();
		calcCentroid();
	}
	
	/**
	 * a copy constructor for a polygon
	 * @param p a non null polygon
	 * @throws NullPointerException if the given polygon is null
	 */
	public Polygon(Polygon p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot create a polygon from a null polygon");
		this.vertices = new Vector2d[p.degree()];
		for (int i = 0; i < this.vertices.length; i++) 
			setVectex(i, new Vector2d(p.getVertex(i)));
		this.setBoundingRectangle(new BoundingRectangle(p.getBoundingRectangle()));
		this.setCentroid(p.getCentroid());
	}
	
	/**
	 * the degree of a polygon is the number of vertices it has
	 * @return the degree of the polygon
	 */
	public int degree() { return this.vertices.length; }
	
	/**
	 * returns the index-th vertex of the polygon
	 * @param index the index of the vertex
	 * @return a Vector2d of the vertex
	 * @throws IndexOutOfBoundsException if the vertex index is out of bounds
	 */
	public Vector2d getVertex(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= this.vertices.length)
			throw new IndexOutOfBoundsException("polygon vetrex index is out of bounds");
		return this.vertices[index];
	}
	
	/**
	 * sets the index-th vertex of the polygon to the given vertex
	 * @param index the index of the vertex we want to set
	 * @param vertex the vertex we want to set
	 * @throws IndexOutOfBoundsException if the vertex index is out of bounds
	 * @throws NullPointerException if the given vertex is null
	 */
	public void setVectex(int index, Vector2d vertex) throws IndexOutOfBoundsException, NullPointerException {
		if (vertex == null) throw new NullPointerException("cannot set a vertex in a polygon to a null vertex");
		if (index < 0 || index > this.vertices.length)
			throw new IndexOutOfBoundsException("polygon vetrex index is out of bounds");
		this.vertices[index] = vertex;
	}
	
	/**
	 * returns a vector equivalent to the index-th edge of the polygon.
	 * the vector is from vertex[index] to vertex[index + 1], except for the last
	 * vector which is from vertex[index] to vertex[0]
	 * @param index the index of the edge
	 * @return a vector equivalent to the edge in the direction of the higher ordered vertex
	 * @throws IndexOutOfBoundsException if the edge index is out of bounds
	 */
	public Edge getEdge(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= this.vertices.length)
			throw new IndexOutOfBoundsException("polygon vector index is out of bounds");
		int next = (index + 1) % this.degree();
		return new Edge(getVertex(index), getVertex(next));
	}
	
	/**
	 * returns the centroid of the polygon (avg of all the vertices)
	 * @return centroid Vector2d
	 */
	public Vector2d getCentroid() {
		if (this.centroid == null) calcCentroid();
		return this.centroid;
	}
	
	/**
	 * sets the polygons centroid to a given point
	 * @param centroid the polygons centroid
	 * @throws NullPointerException if the given Vector2d is null
	 */
	private void setCentroid(Vector2d centroid) throws NullPointerException {
		if (centroid == null) throw new NullPointerException("cannot set a polygon centroid to a null vector2d");
		this.centroid = centroid;
	}
	
	/**
	 * calculates the polygons centroid by averaging out all the vertices of the polygon
	 */
	public void calcCentroid() {
		if (this.centroid == null) this.centroid = new Vector2d(0, 0);
		else { this.centroid.x(0); this.centroid.y(0); }
		
		for (int i = 0; i < degree(); i++)
			this.centroid = this.centroid.add(getVertex(i));
		
		this.centroid = this.centroid.div(degree());
	}
	
	/**
	 * calculates and returns the area of the polygon
	 * @return the area of the polygon
	 */
	public float area() {
		float area = 0;
		int next;
		for (int i = 0; i < degree(); i++) {
			next = (i + 1) % degree();
			area += getVertex(i).x()*getVertex(next).y() - getVertex(i).y()*getVertex(next).x();
		}
		area /= 2;
		return area;
	}
	
	/**
	 * creates and returns the index-th centroid triangle
	 * a centroid triangle is a triangle were one of its vertices is the polygons centroid,
	 * and the other two are two adjacent vertices in the overall polygon. (vertices index and index + 1)
	 * @param index the index of the centroid triangle
	 * @return the index-th centroid triangle
	 * @throws IndexOutOfBoundsException if the index is larger than the degree of the polygon or lower than 0
	 */
	public Polygon getCentroidTriangle(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= degree())
			throw new IndexOutOfBoundsException("polygon triangle index is out of bounds");
		return new Polygon(new Vector2d[]{
				getCentroid(), 
				getVertex(index),
				getVertex((index + 1) % degree())});
	}
		
	/**
	 * translates (moves) the polygon by the given vector
	 * @param v a translation vector
	 * @throws NullPointerException if the given vector is null
	 */
	public void translate(Vector2d v) throws NullPointerException{
		if (v == null) throw new NullPointerException("Cannot translate polygon by a null vector");
		for (int i = 0; i < degree(); i++)
			setVectex(i, getVertex(i).add(v));
		setCentroid(getCentroid().add(v));
		boundingRectangleDirty = true;
	}

	public void rotate(float radians) {
		Vector2d pos = getCentroid();
		this.translate(pos.mul(-1));
		for (int i = 0; i < this.degree(); i++) {
			//rotate the point about the origin
			this.setVectex(i, 
					new Vector2d(
							Matrix2d.rotationMatrix2d(radians).mul(
									getVertex(i).getMatrix()
									)
							)
					);
		}
		this.translate(pos);
		boundingRectangleDirty = true;
	}
	
	public boolean isConvex() {
		for (int i = 0; i < degree(); i++) {
			if (!getEdge(i).isOnLeft( getVertex((i + 2) % degree()) ))
				return false;
		}
		return true;
	}
	
	private void setBoundingRectangle(BoundingRectangle boundingRectangle) throws NullPointerException {
		if (boundingRectangle == null)
			throw new NullPointerException("cannot set polygon bounding rectangle to null");
		this.boundingRectangle = boundingRectangle;
	}
	
	public void calcBoundingRectangle() {
		if (degree() == 0) {
			setBoundingRectangle(new BoundingRectangle(getCentroid(), new Vector2d(0, 0)));
			boundingRectangleDirty = false;
			return;
		}
		
		float minX = getVertex(0).x(),
			maxX = getVertex(0).x(),
			minY = getVertex(0).y(),
			maxY = getVertex(0).y();
		for (int i = 1; i < degree(); i++) {
			if (getVertex(i).x() < minX) minX = getVertex(i).x();
			if (getVertex(i).x() > maxX) maxX = getVertex(i).x();
			
			if (getVertex(i).y() < minY) minY = getVertex(i).y();
			if (getVertex(i).y() > maxY) maxY = getVertex(i).y();
		}
		Vector2d diagonal = new Vector2d(Math.abs(maxX - minX), Math.abs(maxY - minY));
		Vector2d pos = new Vector2d(minX, minY);
		setBoundingRectangle(new BoundingRectangle(pos, diagonal));
		boundingRectangleDirty = false;
	}
	
	/**
	 * returns an array of size two, containing the points that define the bounding rectangle of the polygon.
	 * the first point is the upper left point, and the second is the bottom right point
	 * @return the bounding rectangle defining points in a Vector2d array of size 2
	 */
	public BoundingRectangle getBoundingRectangle() {
		if (this.boundingRectangle == null || this.boundingRectangleDirty) 
			calcBoundingRectangle();
		return this.boundingRectangle;
	}
	
	@Override
	public String toString() {
		String out = "";
		int i;
		for (i = 0; i < degree() - 1; i++)
			out += getVertex(i).toString() + ", ";
		out += getVertex(i).toString();
		return out;
	}
	
	public String toRoundedString(int decimalPoints) {
		String out = "";
		int i;
		for (i = 0; i < degree() - 1; i++)
			out += getVertex(i).toRoundedString(decimalPoints) + ", ";
		out += getVertex(i).toRoundedString(decimalPoints);
		return out;
	}
	
	
	/**
	 * creates a rectangle polygon of the given width and height
	 * @param width width of the rectangle
	 * @param height height of the rectangle
	 * @return rectangle polygon
	 * @throws RuntimeException if one or more of the dimensions is not grater than 0
	 */
	public static Polygon getRectangle(float width, float height) throws RuntimeException {
		if (width <= 0 || height <= 0)
			throw new RuntimeException("rectangle width and height must be grater than 0");
		
		return new Polygon(new Vector2d[]{
				new Vector2d(-width / 2, -height / 2),
				new Vector2d(+width / 2, -height / 2),
				new Vector2d(+width / 2, +height / 2),
				new Vector2d(-width / 2, +height / 2)});
	}
	
	/**
	 * creates a segmented circle of the given radius, with the given amount of segments
	 * @param distanceFromCenter the radius of the circle
	 * @param degree number of segments to "break" the circle to
	 * @return segmented circle polygon
	 * @throws RuntimeException if the radius is not grater than 0, or the number of segments is not grater than 2
	 */
	public static Polygon getRegularPolygon(float distanceFromCenter, int degree) throws RuntimeException {
		if (distanceFromCenter <= 0) throw new RuntimeException("regular polygon distance from center must be grater than 0");
		if (degree <= 2) throw new RuntimeException("regular polygon degree must be grater than 2");
		Vector2d[] vertices = new Vector2d[degree];
		Vector2d movingVertex = new Vector2d(distanceFromCenter, 0);
		float angle = (float)Math.PI * 2 / degree;
		for (int i = 0; i < degree; i++) {
			vertices[i] = new Vector2d(movingVertex);
			movingVertex = movingVertex.rotate(angle);
		}
		return new Polygon(vertices);
	}
}
