package geometry;

import java.util.Vector;

import matrices.*;
import polify.*;

public class IntersectionData {
	private final Polygon clip;
	private final Vector2d MTV;
	private final Vector2d[] scaledMTVs;
	
	public static IntersectionData getIntersectionData(BodyPair bodyPair) throws NullPointerException {
		if (bodyPair == null)
			throw new NullPointerException("cannot create an intersection instance using a null body pair");
		try {
			return new IntersectionData(bodyPair);
		}
		catch (Exception e) {
			return null;
		}
	}
	
	private IntersectionData(BodyPair bodyPair) throws NullPointerException, RuntimeException {
		if (bodyPair == null)
			throw new NullPointerException("cannot create an intersection instance using a null body pair");
		this.clip = calcClip(bodyPair);
		this.MTV = calcMTV(bodyPair);
		this.scaledMTVs = calcScaledMTVs(bodyPair, getMTV());
		if (getClip() == null || getMTV() == null || getScaledMTVs() == null)
			throw new RuntimeException("cannot create an intersection with two bodies that are not intersecting");
	}
	
	public Polygon getClip() { return this.clip; }
	public Vector2d getMTV() { return this.MTV; }
	public Vector2d[] getScaledMTVs() { return this.scaledMTVs; }
	
	
	private Polygon calcClip(BodyPair bodyPair) {
		Vector<Vector2d> output = new Vector<>(), input = new Vector<>();
		for (int i = 0; i < bodyPair.getBodyA().getWorldSpacePolygon().degree(); i++) { 
			output.add(bodyPair.getBodyA().getWorldSpacePolygon().getVertex(i));
		}
		
		//note: i assume that both polygons are defined counterclockwise.
		//because of that, if a point is on the left of an edge, 
		//it is on the "inside side" of the edge (referring to the polygon)
		
		Edge clipEdge; Vector2d start, end;
		for (int ci = 0; ci < bodyPair.getBodyB().getWorldSpacePolygon().degree(); ci++) { //ci = clip index
			if (output.size() == 0) {
				return null;
			}
			clipEdge = bodyPair.getBodyB().getWorldSpacePolygon().getEdge(ci);
			input = new Vector<>(output); output.clear();
			start = input.lastElement();
			for (int ei = 0; ei < input.size(); ei++) {
				end = input.get(ei);
				if (clipEdge.isOnLeft(end)) {
					if (!clipEdge.isOnLeft(start))
						output.add(new Edge(start, end).computeIntersection(clipEdge));
					output.add(end);
				}
				else {
					if (clipEdge.isOnLeft(start))
						output.add(new Edge(start, end).computeIntersection(clipEdge));
				}
				start = new Vector2d(end);
			}
		}
		if (output.size() < 3) {
			return null;
		}
		Vector2d[] vertices = new Vector2d[output.size()];
		for (int i = 0; i < vertices.length; i++) vertices[i] = output.get(i);
		return new Polygon(vertices);
	}
	private Vector2d calcMTV(BodyPair bodyPair) {
		Vector2d MTV = null;
		Polygon pa = bodyPair.getBodyA().getWorldSpacePolygon();
		Polygon pb = bodyPair.getBodyB().getWorldSpacePolygon();
		Vector2d tempPushVector;
		
		//Entry.clearBuffer();
		//Entry.renderPolygon(pa, Color.cyan);
		//Entry.renderPolygon(pb, Color.blue);
		//Entry.renderBuffer();
		//Entry.sleepUntillSpace();
		
		//this will contain all the previous checked edges that are not perpendicular to one another
		Vector<Vector2d> edges = new Vector<Vector2d>();
		int i, j = 0;
		for (i = 0; i < pa.degree(); i++) {
			tempPushVector = new Vector2d(-pa.getEdge(i).getVector().y(), pa.getEdge(i).getVector().x());
			if (addToCheckedVectors(edges, tempPushVector)) { //if the edge is not perpendicular to one of the previos edges
				tempPushVector = getPushVector(tempPushVector, pa, pb);
				if (tempPushVector == null) {
					return null;
				}
				if (MTV == null || MTV.magnitude() > tempPushVector.magnitude())
					MTV = tempPushVector;
			}
		}
		for (j = 0; j < pb.degree(); j++) {
			tempPushVector = new Vector2d(-pb.getEdge(j).getVector().y(), pb.getEdge(j).getVector().x());
			if (addToCheckedVectors(edges, tempPushVector)) { //if the edge is not perpendicular to one of the previos edges
				tempPushVector = getPushVector(tempPushVector, pa, pb);
				if (tempPushVector == null) {
					return null;
				}
				if (MTV == null || MTV.magnitude() > tempPushVector.magnitude())
					MTV = tempPushVector;
			}
		}
		
		if (MTV.dot( pb.getCentroid().sub(pa.getCentroid()) ) < 0)
			MTV = MTV.mul(-1);
		
		//Entry.renderVector(MTV, new Vector2d(10, 5), Color.red);
		//Entry.renderBuffer();
		//Entry.sleepUntillSpace();
		
		return MTV;
	}
	private Vector2d[] calcScaledMTVs(BodyPair bodyPair, Vector2d MTV) {
		if (MTV == null) return null;
		
		if (new Vector2d(bodyPair.getBodyA().getCM(), bodyPair.getBodyB().getCM()).dot(getMTV()) > 0)
			MTV = MTV.mul(-1);
		
		float invMassSum = bodyPair.getBodyA().getInvMass() + bodyPair.getBodyB().getInvMass();
		if (invMassSum == 0) {
			return new Vector2d[]{
					MTV.mul(0.5f), 
					MTV.mul(-0.5f)};
		}
		return new Vector2d[]{
				MTV.mul( 1 * bodyPair.getBodyA().getInvMass() / invMassSum), 
				MTV.mul(-1 * bodyPair.getBodyB().getInvMass() / invMassSum)};
	}
	
	public boolean seperate(BodyPair bodyPair) {
		assert(getScaledMTVs().length == 2 && getScaledMTVs()[0] != null && getScaledMTVs()[1] != null);
		
		bodyPair.getBodyA().translate(getScaledMTVs()[0]);
		bodyPair.getBodyB().translate(getScaledMTVs()[1]);
		return true;
	}
	
	
	
	
	
	
	
	private static Vector2d getPushVector(Vector2d v, Polygon a, Polygon b) throws NullPointerException {
		if (v == null) throw new NullPointerException("cannot get push vector using a null vector");
		if (a == null || b == null) throw new NullPointerException("cannot get push vector using null polygons");
		
		float aMin, aMax, bMin, bMax, d;
		d = v.dot(a.getVertex(0));
		aMin = aMax = d;
		for (int i = 0; i < a.degree(); i++) {
			d = v.dot(a.getVertex(i));
			if (d < aMin) 		aMin = d;
			else if (d > aMax) 	aMax = d;
		}
		
		d = v.dot(b.getVertex(0));
		bMin = bMax = d;
		for (int i = 0; i < b.degree(); i++) {
			d = v.dot(b.getVertex(i));
			if (d < bMin) 		bMin = d;
			else if (d > bMax) 	bMax = d;
		}
		
		//if vector separates polygons
		if (aMin > bMax || bMin > aMax) return null;
		
		//find the interval overlap
		float d0 = aMax - bMin;
		float d1 = bMax - aMin;
		float depth = (d0 < d1)? d0: d1;
		
		float axis_length_squared = v.dot(v);
		
		v = v.mul(depth / axis_length_squared);
		return v;
	}
	
	private static boolean addToCheckedVectors(Vector<Vector2d> edges, Vector2d edge) throws NullPointerException {
		if (edges == null) throw new NullPointerException("cannot add an edge to a null edge Vector<>");
		if (edge == null) throw new NullPointerException("cannot add a null edge to the edges Vector<>");
		
		for (int i = 0; i < edges.size(); i++) {
			//if the given edge is perpendicular to the i-th edge in edges
			if (edges.get(i) != null && edges.get(i).dot(new Vector2d(- edge.y(), edge.x())) == 0) {
				return false;
			}
		}
		
		edges.add(edge);
		return true;
	}
	
	/*public static Vector2d MTV(Polygon a, Polygon b) {
		Vector2d tempPushVector, MTV = null;
		//this will contain all the previous checked edges that are not perpendicular to one another
		Vector<Vector2d> edges = new Vector<Vector2d>();
		int i, j = 0;
		for (i = 0; i < a.degree(); i++) {
			tempPushVector = new Vector2d(-a.getEdge(i).getVector().y(), a.getEdge(i).getVector().x());
			if (addToCheckedVectors(edges, tempPushVector)) { //if the edge is not perpendicular to one of the previos edges
				tempPushVector = getPushVector(tempPushVector, a, b);
				if (tempPushVector == null) return null;
				if (MTV == null || MTV.magnitude() > tempPushVector.magnitude())
					MTV = tempPushVector;
			}
		}
		for (j = 0; j < b.degree(); j++) {
			tempPushVector = new Vector2d(-b.getEdge(j).getVector().y(), b.getEdge(j).getVector().x());
			if (addToCheckedVectors(edges, tempPushVector)) { //if the edge is not perpendicular to one of the previos edges
				tempPushVector = getPushVector(tempPushVector, a, b);
				if (tempPushVector == null) return null;
				if (MTV == null || MTV.magnitude() > tempPushVector.magnitude())
					MTV = tempPushVector;
			}
		}
		
		if (MTV.dot( b.getCentroid().sub(a.getCentroid()) ) < 0)
			MTV = MTV.mul(-1);
		
		return MTV;
	}*/
}
