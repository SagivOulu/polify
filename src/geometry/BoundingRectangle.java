package geometry;

import matrices.*;

public class BoundingRectangle {
	private Vector2d pos;
	private Vector2d diagonal; //a vector pointing from the top left to the bottom right
	
	public BoundingRectangle(Vector2d pos, Vector2d diagonal) throws NullPointerException {
		if (pos == null) throw new NullPointerException("cannot create a bounding rectangle with a null pos");
		if (diagonal == null) throw new NullPointerException("cannot create a bounding rectangle with a null diagonal");
		
		setPos(pos);
		setDiagonal(diagonal);
	}
	
	public BoundingRectangle(BoundingRectangle br) throws NullPointerException {
		if (br == null) throw new NullPointerException("cannot create a bounding rectangle using a null bounding rectangle");
		setPos(new Vector2d(br.getPos()));
		setDiagonal(new Vector2d(br.getDiagonal()));
	}
	
	public BoundingRectangle(Polygon p) {
		if (p == null)
			throw new NullPointerException("cannot create a bounding rectangle for a null polygon");
		
		if (p.degree() == 0) {
			setPos(p.getCentroid());
			setDiagonal(new Vector2d(0, 0));
			return;
		}
		
		float minX = p.getVertex(0).x(),
			maxX = p.getVertex(0).x(),
			minY = p.getVertex(0).y(),
			maxY = p.getVertex(0).y();
		for (int i = 1; i < p.degree(); i++) {
			if (p.getVertex(i).x() < minX) minX = p.getVertex(i).x();
			if (p.getVertex(i).x() > maxX) maxX = p.getVertex(i).x();
			
			if (p.getVertex(i).y() < minY) minY = p.getVertex(i).y();
			if (p.getVertex(i).y() > maxY) maxY = p.getVertex(i).y();
		}
		setPos(new Vector2d(minX, minY));
		setDiagonal(new Vector2d(Math.abs(maxX - minX), Math.abs(maxY - minY)));
	}
	
	public void setPos(Vector2d pos) throws NullPointerException {
		if (pos == null) throw new NullPointerException("cannot set bounding rectangle pos to null");
		this.pos = pos;
	}
	
	public Vector2d getPos() { return this.pos; }
	
	public void setDiagonal(Vector2d diagonal) throws NullPointerException {
		if (diagonal == null) throw new NullPointerException("cannot set bounding rectangle diagonal to null");
		//TODO: add a check to see if the x and y values are not negative
		this.diagonal = diagonal;
	}
	
	public Vector2d getDiagonal() { return this.diagonal; }
	
	public void setWidth(float width) throws RuntimeException {
		if (width < 0) throw new RuntimeException("bounding rectangle width must not negative");
		getDiagonal().x(width);
	}
	
	public void setHeight(float height) throws RuntimeException {
		if (height <= 0) throw new RuntimeException("bounding rectangle height must be not negative");
		getDiagonal().x(height);
	}
	
	public float getWidth() throws NullPointerException {
		if (getDiagonal() == null) throw new NullPointerException("cannot get the width of a bounding rectangle with a null diagonal");
		return getDiagonal().x();
	}
	
	public float getHeight() throws NullPointerException {
		if (getDiagonal() == null) throw new NullPointerException("cannot get the height of a bounding rectangle with a null diagonal");
		return getDiagonal().y();
	}

	public Vector2d getVertex(int index) throws IndexOutOfBoundsException {
		switch (index) {
		case 0:
			return new Vector2d(getPos());
		case 1:
			return new Vector2d(getPos().x() + getWidth(), getPos().y());
		case 2:
			return new Vector2d(getPos().x() + getWidth(), getPos().y() + getHeight());
		case 3:
			return new Vector2d(getPos().x(), getPos().y() + getHeight());
		default:
			throw new IndexOutOfBoundsException("a bounding rectangle has only 4 vertices");
		}
	}
	
	public boolean isPointInside(Vector2d v) throws NullPointerException {
		if (v == null) throw new NullPointerException("cannot check isPointInside with a null point");
		return 
			this.getPos().x() <= v.x() && v.x() <= this.getPos().x() + this.getWidth() &&
			this.getPos().y() <= v.y() && v.y() <= this.getPos().y() + this.getHeight();
	}
	
	public static  boolean isIntersecting(BoundingRectangle b1, BoundingRectangle b2) throws NullPointerException {
		if (b1 == null || b2 == null) 
			throw new NullPointerException("cannot check intersection null bounding rectangles");
		
		return (b1.getPos().x() <= b2.getPos().x() + b2.getWidth() && b1.getPos().x() + b1.getWidth() >= b2.getPos().x() &&
				b1.getPos().y() <= b2.getPos().y() + b2.getHeight() && b1.getPos().y() + b1.getHeight() >= b2.getPos().y());
		
	}
}
