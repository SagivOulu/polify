package polify;

import java.util.ArrayList;
import java.util.Vector;

import geometry.BoundingRectangle;
import geometry.IntersectionData;
import geometry.Polygon;
import matrices.Vector2d;
import physics.CollisionData;

public class World {
	private Vector<Body> bodies;
	private Vector2d gravity;
	private CollisionListener collisionListener;
	private BodyRenderer bodyRenderer;
	private Vector<BodyPair> collidableBodyPairs;
	private boolean collidableBodyPairsDirty = true; 
	
	public World(Vector2d gravity, CollisionListener collisionListener, BodyRenderer bodyRenderer) throws NullPointerException {
		if (collisionListener == null)
			throw new NullPointerException("cannot create world with a null collision listener");
		if (bodyRenderer == null)
			throw new NullPointerException("cannot create world with a null body renderer");
		if (gravity == null)
			throw new NullPointerException("cannot set world gravity to a null Vector2d");
		
		this.bodies = new Vector<Body>();
		setGravity(gravity);
		
		
		setCollionListener(collisionListener);
		setBodyPainter(bodyRenderer);
	}
	
	public void setCollionListener(CollisionListener colList) throws NullPointerException {
		if (colList == null)
			throw new NullPointerException("cannot set world collision listener to null");
		this.collisionListener = colList;
	}
	
	public CollisionListener getCollisionListener() { return this.collisionListener; }
	
	public void setBodyPainter(BodyRenderer bodyRenderer) throws NullPointerException {
		if (bodyRenderer == null) throw new NullPointerException("cannot set world body renderer to null");
		this.bodyRenderer = bodyRenderer;
	}
	
	public BodyRenderer getBodyRenderer() { return this.bodyRenderer; }
	
	public void render() {
		for (int i = 0; i < bodyCount(); i++) {
			getBodyRenderer().renderBody(getBody(i));
		}
	}
	
	public void addBounderies(Vector2d topLeft, Vector2d bottomRight, float width, 
			int topCategory, int bottomCategory, int rightCategory, int leftCategory) throws NullPointerException, RuntimeException {
		if (width <= 0) throw new RuntimeException("world bounderies width must be grater than 0");
		if (topLeft == null || bottomRight == null)
			throw new NullPointerException("cannot add bounderies to a world using null Vector2ds");
		
		float dx = Math.abs(topLeft.x() - bottomRight.x());
		float dy = Math.abs(topLeft.y() - bottomRight.y());
		Body top, bottom, right, left;
		Polygon verticalWall, horizontalWall;
		verticalWall = Polygon.getRectangle(width, dy + 2 * width);
		horizontalWall = Polygon.getRectangle(dx + 2 * width, width);
		
		top = new Body(new Polygon(horizontalWall));
		top.setPosition(topLeft.add(new Vector2d(dx / 2, -width / 2)));
		top.setType(Body.STATIC);
		top.setCategories(topCategory);
		top.setCollisionFilter(top.getCollisionFilter() ^ bottomCategory ^ rightCategory ^ leftCategory);
		top.setUserData("topWall");
		
		bottom = new Body(new Polygon(horizontalWall));
		bottom.setPosition(bottomRight.sub(new Vector2d(dx / 2, -width / 2)));
		bottom.setType(Body.STATIC);
		bottom.setCategories(bottomCategory);
		bottom.setCollisionFilter(bottom.getCollisionFilter() ^ topCategory ^ rightCategory ^ leftCategory);
		bottom.setUserData("bottomWall");
		
		right = new Body(new Polygon(verticalWall));
		right.setPosition(bottomRight.add(new Vector2d(width / 2, -dy / 2)));
		right.setType(Body.STATIC);
		right.setCategories(rightCategory);
		right.setCollisionFilter(right.getCollisionFilter() ^ topCategory ^ bottomCategory ^ leftCategory);
		right.setUserData("rightWall");
		
		left = new Body(new Polygon(verticalWall));
		left.setPosition(topLeft.add(new Vector2d(-width / 2, dy / 2)));
		left.setType(Body.STATIC);
		left.setCategories(leftCategory);
		left.setCollisionFilter(left.getCollisionFilter() ^ topCategory ^ bottomCategory ^ rightCategory);
		left.setUserData("leftWall");
		
		this.addBody(top); this.addBody(bottom); this.addBody(left); this.addBody(right);
	}
	
	public void setGravity(Vector2d gravity) throws NullPointerException {
		if (gravity == null) throw new NullPointerException("cannot set world gravity to a null Vector2d");
		this.gravity = gravity;
	}
	
	public Vector2d getGravity() { return this.gravity; }
	
	public Body getBody(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= this.bodies.size())
			throw new IndexOutOfBoundsException("world index of body to get is out of bounds");
		return this.bodies.get(index);
	}
	
	public int getBodyIndex(Body b) throws NullPointerException {
		if (b == null) throw new NullPointerException("cannot get the index of a null body in the world");
		for (int i = 0; i < bodyCount(); i++) {
			if (getBody(i).compareTo(b) == 0) {
				return i;
			}
		}
		return -1;
	}
	
	public Body[] getBodiesByType(String type) throws NullPointerException {
		if (type == null) throw new NullPointerException("cannot get bodies from world using a null type string");
		ArrayList<Body> bodies = new ArrayList<>();
		for (int i = 0; i < this.bodyCount(); i++) {
			if (getBody(i).getUserData() == type) bodies.add(getBody(i));
		}
		return bodies.toArray(new Body[0]);
	}
	
	public void setBody(int index, Body body) throws IndexOutOfBoundsException, NullPointerException {
		if (index < 0 || index >= this.bodies.size())
			throw new IndexOutOfBoundsException("world index of body to set is out of bounds");
		if (body == null) throw new NullPointerException("cannot set a world body to a null body");
		this.bodies.set(index, body);
	}
	
	public void addBody(Body body) throws NullPointerException {
		if (body == null) throw new NullPointerException("cannot add a null body to a world");
		this.bodies.add(body);
		
		
		//gatherCollidableBodyPairs(); //TODO: find a better place to put this in
	}
	
	public void removeBody(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= this.bodies.size())
			throw new IndexOutOfBoundsException("world index of body to remove is out of bounds");
		this.bodies.remove(index);
	}
	
	public boolean removeBody(Body b) throws NullPointerException {
		if (b == null) throw new NullPointerException("cannot remove a null body from world");
		int index = getBodyIndex(b);
		if (index == -1) return false;
		
		removeBody(index);
		return true;
	}
	
	public int bodyCount() { return this.bodies.size(); }
	
	public void gatherCollidableBodyPairs() {
		if (this.collidableBodyPairs == null) this.collidableBodyPairs = new Vector<>();
		else this.collidableBodyPairs.clear();
		for (int i = 0; i < bodyCount(); i++) {
			for (int j = i + 1; j < bodyCount(); j++) {
				if ((getBody(i).getCollisionFilter() & getBody(j).getCategories()) == 0 ||
					(getBody(j).getCollisionFilter() & getBody(i).getCategories()) == 0) continue;
				collidableBodyPairs.add(new BodyPair(getBody(i), getBody(j)));
			}
		}
		this.collidableBodyPairsDirty = false;
	}
	
	public Vector<BodyPair> getCollidableBodyPairs() {
		if (this.collidableBodyPairs == null || this.collidableBodyPairsDirty) 
			gatherCollidableBodyPairs();
		return this.collidableBodyPairs;
	}
	
	public Vector<BodyPair> collisionCheckBroadPhase(Vector<BodyPair> bodyPairs) {
		Vector<BodyPair> out = new Vector<>();
		Vector2d zeroVector = new Vector2d(0, 0);
		for (int i = 0; i < bodyPairs.size(); i++) {
			if (!BoundingRectangle.isIntersecting(
					bodyPairs.get(i).getBodyA().getWorldSpacePolygon().getBoundingRectangle(), 
					bodyPairs.get(i).getBodyB().getWorldSpacePolygon().getBoundingRectangle())) continue;
			if (bodyPairs.get(i).getBodyA().getVelocity().isEqual(zeroVector) &&
					bodyPairs.get(i).getBodyB().getVelocity().isEqual(zeroVector) &&
					bodyPairs.get(i).getBodyA().getAngularVelocity() == 0 && 
					bodyPairs.get(i).getBodyB().getAngularVelocity() == 0)
					continue;
			out.add(bodyPairs.get(i));
		}
		
		return out;
	}
	
	public void simulate(float dt) throws RuntimeException {
		for (int i = 0; i < bodyCount(); i++) {
			if (getBody(i).getType() == Body.DYNAMIC) {
				getBody(i).applyForce(getGravity().mul(getBody(i).getMass()), null);
			}
		}
		
		Vector<BodyPair> bodyPairs = collisionCheckBroadPhase(getCollidableBodyPairs());
		
		//System.out.println("size after broad phase = " + bodyPairs.size());
		
		IntersectionData intersectionData; CollisionData collisionData; Float COR;
		
		for (int i = 0; i < bodyPairs.size(); i++) {
			intersectionData = IntersectionData.getIntersectionData(bodyPairs.get(i));
			if (intersectionData == null) continue;
			COR = getCollisionListener().preSolve(bodyPairs.get(i), intersectionData);
			if (COR == null) continue;
			if (bodyPairs.get(i).getBodyA().getType() == Body.DYNAMIC || bodyPairs.get(i).getBodyB().getType() == Body.DYNAMIC) {
				collisionData = CollisionData.getCollisionData(bodyPairs.get(i), intersectionData, COR);
				if (collisionData != null) {
					collisionData.simulate(bodyPairs.get(i));
					intersectionData.seperate(bodyPairs.get(i));
					getCollisionListener().postSolve(bodyPairs.get(i));
				}
			}
		}
		
		/*
		for (int b1i = 0; b1i < bodyCount(); b1i++) { //b1i = body 1 index
			b1 = getBody(b1i);
			for (int b2i = b1i + 1; b2i < bodyCount(); b2i++) { //body 2 index
				b2 = getBody(b2i);
				//check both body's filters to see if they collide
				if ((b1.getCollisionFilter() & b2.getCategories()) == 0 ||
					(b2.getCollisionFilter() & b1.getCategories()) == 0) continue;
				if (b1.getVelocity().isEqual(zeroVector) && b2.getVelocity().isEqual(zeroVector) &&
					b1.getAngularVelocity() == 0 && b2.getAngularVelocity() == 0) continue;
				if (!BoundingRectangle.isIntersecting(
						b1.getWorldSpacePolygon().getBoundingRectangle(), 
						b2.getWorldSpacePolygon().getBoundingRectangle())) continue;
				
				BodyPair bodyPair = new BodyPair(b1, b2);
				
				intersectionData = IntersectionData.getIntersectionData(bodyPair);
				if (intersectionData == null) continue;
				COR = getCollisionListener().preSolve(bodyPair, intersectionData);
				if (COR == null) continue;
				if (bodyPair.getBodyA().getType() == Body.DYNAMIC || bodyPair.getBodyB().getType() == Body.DYNAMIC) {
					collisionData = CollisionData.getCollisionData(bodyPair, intersectionData, COR);
					if (collisionData != null) {
						collisionData.simulate(bodyPair);
						intersectionData.seperate(bodyPair);
						getCollisionListener().postSolve(bodyPair);
					}
				}
			}
		}*/
		for (int i = 0; i < bodyCount(); i++) {
			if (getBody(i).getType() == Body.DYNAMIC || getBody(i).getType() == Body.KINEMATIC) {
				getBody(i).step(dt);
			}
		}
	}

	public static int matchCategories(Body b1, Body b2, int cat1, int cat2) {
		if ((b1.getCategories() & cat1) != 0 && (b2.getCategories() & cat2) != 0) return 1;
		if ((b1.getCategories() & cat2) != 0 && (b2.getCategories() & cat1) != 0) return -1;
		return 0;
	}
}