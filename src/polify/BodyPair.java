package polify;

public class BodyPair {
	private Body b1, b2;
	
	public BodyPair(Body b1, Body b2) throws NullPointerException {
		if (b1 == null || b2 == null)
			throw new NullPointerException("cannot create a body pair using null bodies");
		setBodyA(b1);
		setBodyB(b2);
	}
	
	public Body getBodyA() { return b1; }
	public Body getBodyB() { return b2; }
	public void setBodyA(Body b1) throws NullPointerException {
		if (b1 == null) throw new NullPointerException("cannot set intersection body to a null body");
		this.b1 = b1;
	}
	public void setBodyB(Body b2) throws NullPointerException {
		if (b2 == null) throw new NullPointerException("cannot set intersection body to a null body");
		this.b2 = b2;
	}

	public int matchCategories(int categoryA, int categoryB) {
		if ((getBodyA().getCategories() & categoryA) != 0 && (getBodyB().getCategories() & categoryB) != 0)
			return 1;
		if ((getBodyA().getCategories() & categoryB) != 0 && (getBodyB().getCategories() & categoryA) != 0)
			return -1;
		return 0;
	}
}
