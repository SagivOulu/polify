package polify;

import geometry.Polygon;
import matrices.*;
import physics.Impulse;

/**
 * this is a class representing a physical non deforming body, in the shape of a convex polygon.
 * the body has physical attributes, like position, speed, acceleration, angle, angularSpeed, angularAcceleration, mass and 
 * rotational inertia.
 * the body can perform various physical simulations, like collision response and updating itself forward in time.
 * @author sagiv
 *
 */
public class Body implements Comparable<Body> {
	public static final char STATIC = 0;
	public static final char KINEMATIC = 1;
	public static final char DYNAMIC = 2;
	
	private static int idCount = 0;
	
	private char type = Body.DYNAMIC;
	
	private int id;
	private Object userData;
	private int categories = 1;
	private int collisionFilter = 0xFFFFFFFF;
	
	private Polygon bodySpacePolygon;					//body space
	private Polygon worldSpacePolygon = null;			//world space
	private boolean worldSpacePolygonDirty = true;
	
	private Vector2d position = new Vector2d(0, 0);		//world space
	private Vector2d velocity = new Vector2d(0, 0);		//world space
	private Vector2d acceleration = new Vector2d(0, 0);	//world space
	
	private float invMass = -1;
	
	private float angle = 0;							//world space
	private float angularVelocity = 0;					//world space
	private float angularAcceleration = 0;				//world space
	
	private float invInertia = -1;
	
	private Vector2d forcesSum = new Vector2d(0, 0);	//world space
	
	private float torqueSum = 0;						//world space
	
	//************************************************CONSTRUCTORS************************************************
	
	/**
	 * constructs a new body using the given polygon and a mass. 
	 * the center of mass of the body will be set to 0,0 (the center of the polygon)
	 * if the mass is non positive, the body will have infinite mass, and will not be affected by forces and collisions
	 * @param bodySpacePolygon a convex polygon centered about point 0,0
	 * @param mass the mass of the body. if not positive (0 or lower) the body will have infinite mass
	 * @throws NullPointerException if the polygon is null
	 */
	public Body(Polygon bodySpacePolygon) throws NullPointerException, RuntimeException {
		if (bodySpacePolygon == null) throw new NullPointerException("Body must have a non-null polygon");
		if (!bodySpacePolygon.isConvex()) throw new RuntimeException("Body polygon must be convex");
		this.setBodySpacePolygon(bodySpacePolygon);
		
		this.setType(Body.DYNAMIC);
		
		setMass(1);
		
		calculateInertia();
		calcWorldSpacePolygon();
		this.id = Body.idCount++;
	}
	
	/**
	 * constructs a body given a polygon, the center of mass of the polygon and the mass of the body
	 * if the mass is non positive, the body will have infinite mass, and will not be affected by forces and collisions
	 * @param worldSpacePolygon a convex polygon
	 * @param worldSpaceCM the center mass of the polygon
	 * @param mass the mass of the body. if not positive (0 or lower) the body will have infinite mass
	 * @throws NullPointerException if one or more of the given objects are null
	 */
	public Body(Polygon worldSpacePolygon, Vector2d worldSpaceCM) throws NullPointerException, RuntimeException {
		if (worldSpaceCM == null) throw new NullPointerException("Body must have a non-null CM");
		if (worldSpacePolygon == null) throw new NullPointerException("Body must have a non-null polygon");
		if (!worldSpacePolygon.isConvex()) throw new RuntimeException("Body polygon must be convex");
		
		this.bodySpacePolygon = worldSpacePolygon;
		this.bodySpacePolygon.translate(worldSpaceCM.mul(-1));
		
		this.setPosition(new Vector2d(worldSpaceCM));
		
		this.setType(Body.DYNAMIC);
		
		setMass(1);
		
		calculateInertia();
		calcWorldSpacePolygon();
		this.id = Body.idCount++;
	}
	
	/**
	 * constructs a copy of the given body
	 * @param b a body
	 * @throws NullPointerException if the given body is null
	 */
	public Body(Body b) throws NullPointerException {
		if (b == null) throw new NullPointerException("cannot create a body using a null body");
		this.setBodySpacePolygon(new Polygon(b.getBodySpacePolygon()));
		
		this.setInvMass(b.getInvMass());
		this.setInvInertia(b.getInvInertia());
		
		this.setPosition(new Vector2d(b.getPosition()));
		this.setVelocity(new Vector2d(b.getVelocity()));
		this.setAcceleration(new Vector2d(b.getAcceleration()));
		
		this.setAngle(b.getAngle());
		this.setAngularVelocity(b.getAngularVelocity());
		this.setAngularAcceleration(b.getAngularAcceleration());
		
		this.setWorldSpacePolygon(b.getWorldSpacePolygon());
		
		this.setCategories(b.getCategories());
		this.setCollisionFilter(b.getCollisionFilter());
		
		this.setUserData(b.getUserData()); //TODO: find a way to copy the object and not pass the reference
		this.applyForce(b.getForcesSum(), null);
		this.applyTorqueAboutCM(b.getTorqueSum());
		
		this.id = Body.idCount++;
	}
	
	//************************************************POLYGON************************************************
	
	/**
	 * sets the body's body coordinate polygon to the given polygon.
	 * the center of mass of the polygon will be at point 0,0 in body coordinates.
	 * 
	 * the original polygon is the body's polygon before all of its modifications (rotation, translation...).
	 * the center of mass of the original polygon is 0,0
	 * @param p the original polygon
	 */
	public void setBodySpacePolygon(Polygon p) { 
		this.bodySpacePolygon = p;
		worldSpacePolygonDirty = true;
	} 
	
	/**
 	 * sets the body's body coordinate polygon to the given polygon.
	 * the center of mass of the polygon will be at point 0,0 in body coordinates.
	 * 
	 * the original polygon is the body's polygon before all of its modifications (rotation, translation...).
	 * the center of mass of the original polygon is 0,0 
	 * @return the original polygon
	 */
	public Polygon getBodySpacePolygon() { return this.bodySpacePolygon; }
	
	/**
	 * returns the polygon of the body in its position and orientation
	 * the world polygon is the original polygon, after translation and rotation to the body's position
	 * @return the world polygon
	 */
	public Polygon getWorldSpacePolygon() {
		if (this.worldSpacePolygon == null || worldSpacePolygonDirty)
			calcWorldSpacePolygon();
		return this.worldSpacePolygon;
	}
	
	/**
	 * sets the body's world polygon to the given polygon
	 * the world polygon is the original polygon, after translation and rotation to the body's position
	 * @param p the world polygon
	 * @throws NullPointerException if the given polygon is null
	 */
	private void setWorldSpacePolygon(Polygon p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot set body world polygon to a null polygon");
		this.worldSpacePolygon = p;
		worldSpacePolygonDirty = false;
	}
	
	/**
	 * calculates the body's world polygon by translating and rotating the 
	 * original polygon to the necessary position and orientation.
	 */
	public void calcWorldSpacePolygon() {
		this.worldSpacePolygon = new Polygon(getBodySpacePolygon());
		this.worldSpacePolygon.rotate(getAngle());
		this.worldSpacePolygon.translate(getPosition());
		worldSpacePolygonDirty = false;
	}
	
	public Vector2d toWorldSpaceVector(Vector2d v) throws NullPointerException {
		if (v == null) throw new NullPointerException("cannot convert a null vector to world space");
		return v.rotate(getAngle());
	}
	
	public Vector2d toBodySpaceVector(Vector2d v) throws NullPointerException {
		if (v == null) throw new NullPointerException("cannot convert a null vector to body space");
		return v.rotate(-getAngle());
	}
	
	public Vector2d toWorldSpacePoint(Vector2d p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot covert null point to world space");
		return p.rotate(getAngle()).add(getPosition());
	}
	
	public Vector2d toBodySpacePoint(Vector2d p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot covert null point to world space");
		return p.sub(getPosition()).rotate(-getAngle());
	}

	//************************************************LINEAR PROPERTIES************************************************
	
	/**
	 * translate (move) the body be a given translation vector
	 * @param v translation vector
	 */
	public void translate(Vector2d v) { 
		setPosition(getPosition().add(v));
		//calcWorldSpacePolygon();
		worldSpacePolygonDirty = true;
	}
	
	/**
	 * sets the position of the bodies center of mass to a given vector
	 * @param position a Vector2d representing the position
	 * @throws NullPointerException if the position vector is null
	 */
	public void setPosition(Vector2d position) throws NullPointerException {
		if (position == null) throw new NullPointerException("cannot set body position to a null vector");
		this.position = position;
		//calcWorldSpacePolygon();
		worldSpacePolygonDirty = true;
	}

	/**
	 * returns the position of the bodies center of mass
	 * @return Vector2d representing position
	 */
	public Vector2d getPosition() { return this.position; }
	
	/**
	 * returns the center of mass (CM) of the body.
	 * note - this method is equivalent to getPosition(). it exists for easier code reading
	 * @return the position (CM) of the body
	 */
	public Vector2d getCM() { return getPosition(); }
	
	/**
	 * returns the velocity of the body at its center of mass
	 * @return velocity vector
	 */
	public Vector2d getVelocity() { return this.velocity; }
	
	/**
	 * returns the velocity of the body at a given position on the body.
	 * @param point a point on the body
	 * @return a velocity vector
	 * @throws NullPointerException if the given point is null
	 */
	public Vector2d getVelocityAt(Vector2d point) throws NullPointerException {
		if (point == null)
			throw new NullPointerException("cannot get body velocity at a null position");
		
		return getVelocity().add(   (new Vector2d(getCM(), point)).rotate((float)Math.PI / 2).mul(getAngularVelocity())   );
	}
	
	/**
	 * sets the body's velocity at its center of mass
	 * @param velocity a velocity vector
	 * @throws NullPointerException if the given vector is null
	 */
	public void setVelocity(Vector2d velocity) throws NullPointerException {
		if (velocity == null) throw new NullPointerException("cannot set a bodies velocity to a null Vector2d object");
		this.velocity = velocity;
	}
	
	/**
	 * returns the body's acceleration vector at its center of mass
	 * @return an acceleration vector
	 */
	public Vector2d getAcceleration() { return this.acceleration; }
	
	/**
	 * sets the body's acceleration vector at its center of mass
	 * @param acceleration an acceleration vector
	 * @throws NullPointerException if the given acceleration vector is null
	 */
	public void setAcceleration(Vector2d acceleration) throws NullPointerException {
		if (acceleration == null) throw new NullPointerException("cannot set a bodies acceleration to a null vector");
		this.acceleration = acceleration;
	}
	
	/**
	 * returns the body's mass. if the body has infinite mass a -1 will be returned
	 * @return the body's mass as a float
	 */
	public float getMass() {
		if (getInvMass() != 0)
			return 1 / this.getInvMass();
		return -1;
	}
	
	/**
	 * returns the inverse mass of the body (1 / mass).
	 * a body with infinite mass will have an inverse mass of 0 (because 1 / infinity = 0)
	 * @return the body's inverse mass
	 */
	public float getInvMass() { return invMass; }
	
	/**
	 * sets the body's mass
	 * @param mass a positive float representing the body's mass
	 * @throws RuntimeException if the mass is not grater than 0
	 */
	public void setMass(float mass) throws RuntimeException {
		if (mass <= 0) throw new RuntimeException("body mass must be grater than 0");
		this.invMass = 1 / mass;
		this.calculateInertia();
	}
	
	/**
	 * sets the body's inverse mass (1 / mass) to the given value.
	 * a body with infinite mass will have an inverse mass of 0 (because 1 / infinity = 0)
	 * @param invMass a non negative float representing the body's inverse mass
	 * @throws RuntimeException if the inverse mass is negative
	 */
	private void setInvMass(float invMass) throws RuntimeException {
		if (invMass < 0)
			throw new RuntimeException("body invMass must be non negative");
		this.invMass = invMass;
		calculateInertia();
	}
	
	
	//************************************************ANGULAR PROPERTIES************************************************
	
	/**
	 * rotates the body a given amount of degrees in radians.
	 * the positive direction is counterclockwise
	 * @param radians the degrees to rotate in radians
	 */
	public void rotate(float radians) {
		this.angle += radians;
		//calcWorldSpacePolygon();
		worldSpacePolygonDirty = true;
	}
	
	/**
	 * sets the angle of the body. the body's angle is the angle between an axis of the body's
	 * polygon in body space and an axis of the body's polygon in world space
	 * @param angle the angle of the body in radians
	 */
	public void setAngle(float angle) {
		this.angle = angle;
		worldSpacePolygonDirty = true;
	}
	
	/**
	 * returns the angle of the body. the body's angle is the angle between an axis of the body's
	 * polygon in body space and an axis of the body's polygon in world space
	 * @return the angle the angle of the body in radians
	 */
	public float getAngle() { return this.angle; }
	
	/**
	 * sets the angular velocity of the body in radians/second
	 * the positive direction is counterclockwise
	 * @param angularVelocity the angular velocity of the body
	 */
	public void setAngularVelocity(float angularVelocity) { this.angularVelocity = angularVelocity; }
	
	/**
	 * returns the angular velocity of the body in radians/second
	 * the positive direction is counterclockwise
	 * @return the angular velocity of the body
	 */
	public float getAngularVelocity() { return this.angularVelocity; }

	/**
	 * sets the angular acceleration of the body in radians/second^2)
	 * the positive direction is counterclockwise
	 * @param angularAcceleration the angular acceleration
	 */
	public void setAngularAcceleration(float angularAcceleration) { this.angularAcceleration = angularAcceleration; }
	
	/**
	 * sets the angular acceleration of the body in radians/second^2)
	 * the positive direction is counterclockwise
	 * @return the angular acceleration of the body
	 */
	public float getAngularAcceleration() { return this.angularAcceleration; }
	
	/**
	 * sets the rotational inertia of the body around the center of mass
	 * @param inertia a float
	 * @throws RuntimeException if the inertia is not grater than 0
	 */
	public void setInertia(float inertia) throws RuntimeException {
		if (inertia <= 0) throw new RuntimeException("a bodies inertia must be grater than 0");
		this.invInertia = 1 / inertia;
	}
	
	/**
	 * returns the rotational inertia of the body around the center of mass
	 * @return inertia of the body
	 */
	public float getInertia() {
		if (getInvInertia() == 0) return -1;
		return 1 / getInvInertia();
	}
	
	/**
	 * returns the inverse of the rotational inertia (1 / inertia) of the body around the center of mass
	 * @return the inverse rotational inertia
	 */
	public float getInvInertia() {
		if (invInertia < 0) this.calculateInertia();
		return invInertia;
	}
	
	/**
	 * sets the inverse of the rotational inertia (1 / inertia) of the body around the center of mass
	 * @param invInertia the inverse rotational inertia
	 * @throws RuntimeException if the inverse inertia is not grater than 0
	 */
	public void setInvInertia(float invInertia) throws RuntimeException {
		if (invInertia < 0) throw new RuntimeException("body invInertia must be non negative");
		this.invInertia = invInertia;
	}
	
	/**
	 * calculates the rotational inertia of the body given its shape and mass
	 */
	public void calculateInertia() {
		float totalInvInertia = 0;
		float triangleUnitInertia, //inertia of a triangle with mass/area = 1 
			triangleArea = 0;
		float distance;
		float a, b, h, d; //parameters calculated from each triangle to calculate each triangles inertia
		Polygon triangle;
		for (int index = 0; index < getBodySpacePolygon().degree(); index++) {
			triangle = getBodySpacePolygon().getCentroidTriangle(index);
			triangleArea = triangle.area();
			
			b = triangle.getEdge(0).getVector().magnitude();
			h = (2 * triangleArea) / b;
			d = triangle.getEdge(1).getVector().magnitude();
			a = (float)Math.sqrt(d*d - h*h);
			triangleUnitInertia = ( b * ( h * ( b*b - b * a + a + h*h))) / 36;
			
			distance = triangle.getCentroid().sub(getPosition()).magnitude();
			
			totalInvInertia += triangleUnitInertia + triangleArea * Math.pow(distance, 2);
		}
		
		totalInvInertia = getInvMass() * triangleArea * 1 / totalInvInertia;
		
		setInvInertia(totalInvInertia);
	}
	
	//************************************************FORCES+************************************************
	
	public void applyWorldSpaceImpulse(Impulse p) throws NullPointerException {
		if (p == null) throw new NullPointerException("cannot apply a null impulse on a body");
		
		setVelocity(getVelocity().add(p.getLinear().mul(getInvMass())));
		setAngularVelocity(getAngularVelocity() + p.getAngular() * getInvInertia());
	}
	
	public void applyForce(Vector2d force, Vector2d bodySpaceOrigin) throws NullPointerException {
		if (force == null) throw new NullPointerException("cannot add a null Vector2d force to a body");
		
		this.forcesSum = this.forcesSum.add(force);
		
		if (bodySpaceOrigin != null) {
			this.torqueSum += (bodySpaceOrigin.rotate(getAngle()).rotate((float)Math.PI / 2)).dot(force);
		}
	}
	
	public void applyTorqueAboutCM(float torque) { this.torqueSum += torque; }
	
	public Vector2d getForcesSum() { return this.forcesSum; }
	
	public float getTorqueSum() { return this.torqueSum; } 
	
	/**
	 * clears all the forces (linear and angular forces) applied to the body
	 */
	public void clearForces() {
		this.forcesSum.x(0); this.forcesSum.y(0);
		this.torqueSum = 0;
	}
	
	//************************************************OTHER************************************************
	
	/**
	 * updates the body's angular and linear positions, velocities and accelerations, to what they will be after dt milliseconds
	 * @param dt milliseconds to advance
	 */
	public void step(float dt) {
		float t = dt / 1000;
		
		if (getType() == Body.DYNAMIC) {
			setAcceleration(getForcesSum().mul(getInvMass()));
			setAngularAcceleration(getTorqueSum() * getInvInertia());
		}
		
		//p(t) = (a * t^2 / 2) + v(0)*t + p(0)
		setPosition((getAcceleration().mul((float)Math.pow(t, 2) / 2)).add(getVelocity().mul(t)).add(getPosition()));
		//v(t) = at + v(0)
		setVelocity(getAcceleration().mul(t).add(getVelocity()));
		
		//note - a prefix to note angular
		
		//av(t) = aa * t + av(0)
		setAngularVelocity(getAngularAcceleration() * t + getAngularVelocity());
		//angle(t) = (aa * t^2 / 2) + av(0)*t + angle(0)
		setAngle((getAngularAcceleration() * (float)Math.pow(t, 2) / 2) + getAngularVelocity() * t + getAngle());
		
		clearForces();
	}
	
	
	
	
	
	
	
	/**
	 * returns the id of the body
	 * @return body's id
	 */
	public int getID() { return this.id; }
	
	/**
	 * sets the type of the body. the body's type is a string meant to help developers classify different bodies in there simulation
	 * @param type the body's type. a string
	 * @throws NullPointerException if the given string is null
	 */
	public void setUserData(Object object) { this.userData = object; }
	
	/**
	 * sets the type of the body. the body's type is a string meant to help developers classify different bodies in there simulation
	 * @return the body's type. a string
	 */
	public Object getUserData() { return this.userData; } 
	
	public void setCategories(int categories) { this.categories = categories; }
	
	public void addCategories(int categories) { this.categories = this.categories | categories; }
	
	public int getCategories() { return this.categories; }
	
	public void setCollisionFilter(int filter) { this.collisionFilter = filter; }
	
	public int getCollisionFilter() { return this.collisionFilter; }
	
	public char getType() { return this.type; }
	
	public void setType(char type) {
		this.type = type;
		if (type == Body.STATIC || type == Body.KINEMATIC) {
			this.setInvMass(0);
		}
	}
	
	@Override
 	public int compareTo(Body b) { return this.id == b.id? 0: 1; }
}