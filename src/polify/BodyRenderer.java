package polify;

public interface BodyRenderer {
	public void renderBody(Body b);
}
