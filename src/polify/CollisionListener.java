package polify;

import geometry.IntersectionData;

public interface CollisionListener {
	public Float preSolve(BodyPair bodyPair, IntersectionData intersectionData);
	public void postSolve(BodyPair bodyPair);
	
	//public void beginContact(Body b1, Body b2);
	//public void endContact(Body b1, Body b2);
}
