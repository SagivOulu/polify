package utils;

import matrices.*;

public class Viewport {
	private float PPM; //pixels per meter;
	private Vector2d origin;
	
	public Viewport(float PPM, Vector2d origin) throws NullPointerException, RuntimeException {
		if (origin == null)
			throw new NullPointerException("cannot create a viewport using a null origin Vector2d");
		if (PPM == 0)
			throw new RuntimeException("cannot create a viewport with a pixel per meter scale of 0");
		
		setPPM(PPM);
		setOrigin(origin);
	}
	
	public void setPPM(float PPM) throws RuntimeException {
		if (PPM == 0)
			throw new RuntimeException("cannot set viewport pixel per meter value to 0");
		this.PPM = PPM;
	}
	
	public float getPPM() { return this.PPM; }
	
	public void setOrigin(Vector2d origin) throws NullPointerException {
		if (origin == null)
			throw new NullPointerException("cannot set viewport origin to a null Vector2d");
		this.origin = origin;
	}
	
	public Vector2d getOrigin() { return this.origin; }
	
	public int toScreenX(float worldX) {
		return (int)(getOrigin().x() + worldX * getPPM());
	}
	
	public int toScreenY(float worldY) {
		return (int)(getOrigin().y() + worldY * getPPM());
	}
}
