package utils;

public class SequentialMatrix {
	private long order = 0;
	private SequentialMatrixCell master;
	private SequentialMatrixCell[][] matrix;
	private int rows, cols;
	
	public SequentialMatrix(int rows, int cols, float masterValue) throws RuntimeException {
		if (rows <= 0 || cols <= 0) throw new RuntimeException("sequential matrix dimentions must be grater than 0");
		this.rows = rows; this.cols = cols;
		matrix = new SequentialMatrixCell[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				set(i, j, masterValue);
			}
		}
		setMaster(masterValue);
	}
	
	public void setMaster(float value) { this.master.set(value, order++); }
	
	public float getMaster() { return this.master.getElement(); }
	
	public void set(int row, int col, float value) throws IndexOutOfBoundsException {
		if (row < 0 || row >= getRows() || col < 0 || col >= getCols())
			throw new IndexOutOfBoundsException("sequential matrix set indexes out of bounds");
		matrix[row][col] = new SequentialMatrixCell(value, order++);
	}
	
	public float get(int row, int col) throws IndexOutOfBoundsException {
		if (row <= 0 || row >= getRows() || col <= 0 || col >= getCols())
			throw new IndexOutOfBoundsException("sequential matrix get indexes out of bounds");
		return matrix[row][col].getOrder() > master.getOrder()? 
				matrix[row][col].getElement() : master.getElement();
	}
	
	public int getRows() { return this.rows; }
	
	public int getCols() { return this.cols; }
}