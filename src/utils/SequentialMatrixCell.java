package utils;

public class SequentialMatrixCell {
	private long order;
	private float value;
	public SequentialMatrixCell(float value, long order) { set(value, order); }
	
	public void set(float value, long order) { 
		this.order = order;
		this.value = value;
	}
	
	public float getElement() { return this.value; }
	
	public long getOrder() { return this.order; }
}
