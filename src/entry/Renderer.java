package entry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import polify.*;
import utils.*;

public class Renderer implements BodyRenderer {
	private Graphics g;
	private BufferedImage bufferImage;
	private Viewport viewport;
	
	public Renderer(Graphics g, BufferedImage bufferImage, Viewport viewport) throws NullPointerException {
		if (g == null) throw new NullPointerException("cannot create a Renderer with a null Graphics");
		if (bufferImage == null) throw new NullPointerException("cannot create a Renderer with a null buffer image");
		if (viewport == null) throw new NullPointerException("cannot create a renderer using a null viewport");
		
		setGraphics(g);
		setBufferImage(bufferImage);
		setViewport(viewport);
	}
	
	public void setGraphics(Graphics g) throws NullPointerException {
		if (g == null)
			throw new NullPointerException("cannot set Renderer Graphics to null");
		this.g = g;
	}
	
	public Graphics getGraphics() { return this.g; }
	
	public void setBufferImage(BufferedImage bufferImage) throws NullPointerException {
		if (bufferImage == null)
			throw new NullPointerException("cannot set Renderer buffer image to null");
		this.bufferImage = bufferImage;
	}
	
	public BufferedImage getBufferImage() { return this.bufferImage; }
	
	public void setViewport(Viewport viewport) throws NullPointerException {
		if (viewport == null) throw new NullPointerException("cannot set renderer viewport to null");
		this.viewport = viewport;
	}
	
	public Viewport getViewport() { return this.viewport; }
	
	public void renderBuffer() {
		getGraphics().drawImage(getBufferImage(), 0, 0, null);
	}
	
	public void clearBuffer() {
		getBufferImage().getGraphics().clearRect(0, 0, getBufferImage().getWidth(), getBufferImage().getHeight());
	}
	
	@Override
	public void renderBody(Body b) {
		int[] xPoints = new int[b.getWorldSpacePolygon().degree()];
		int[] yPoints = new int[b.getWorldSpacePolygon().degree()];
		assert(xPoints.length == yPoints.length);
		for (int i = 0; i < xPoints.length; i++) {
			xPoints[i] = getViewport().toScreenX(b.getWorldSpacePolygon().getVertex(i).x());
			yPoints[i] = getViewport().toScreenY(b.getWorldSpacePolygon().getVertex(i).y());
		}
		getBufferImage().getGraphics().setColor(Color.gray);
		getBufferImage().getGraphics().drawPolygon(xPoints, yPoints, xPoints.length);
	}
	
}
