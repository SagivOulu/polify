package entry;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import geometry.IntersectionData;
import geometry.Polygon;
import matrices.*;
import polify.Body;
import polify.BodyPair;
import polify.CollisionListener;
import polify.World;
import utils.Viewport;

public class Entry extends JFrame implements CollisionListener, KeyListener {
	private static final long serialVersionUID = 1L;
	
	Renderer renderer;
	Viewport viewport;
	
	boolean right = false, left = false, up = false;
	
	final int DEFAULT_BIT = 1;
	final int BOUNDERY_BIT = 2;
	final int BALL_BIT = 4;
	final int BRICK_BIT = 8;
	final int PADDLE_BIT = 16;
	
	static World world;
	
	Body ball, paddle;
	
	public static void main(String[] args) { new Entry(); }
	
	public Entry() {
		float worldWidth = 20, worldHeight = 10;
		viewport = new Viewport(60, new Vector2d(50, 60));
		//setSize(1900, 1000);
		setSize((int)(viewport.getPPM() * worldWidth) + (int)viewport.getOrigin().x() * 2,
				(int)(viewport.getPPM() * worldHeight) + (int)viewport.getOrigin().y() * 2);
	    setVisible(true);
	    setResizable(false);
	    addKeyListener(this);
	    this.setFocusable(true);
	    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	    
	    
	    renderer = new Renderer(
				getGraphics(), 
				new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB),
				viewport);
	    world = new World(new Vector2d(0, 0f), this, renderer);
		world.addBounderies(new Vector2d(0, 0), new Vector2d(worldWidth, worldHeight), 999, 
				BOUNDERY_BIT, BOUNDERY_BIT, BOUNDERY_BIT, BOUNDERY_BIT);
	    
		ball = new Body(Polygon.getRegularPolygon(0.3f, 30));
		ball.setType(Body.DYNAMIC);
		ball.setCategories(BALL_BIT);
		ball.setCollisionFilter(PADDLE_BIT | BOUNDERY_BIT | BRICK_BIT);
		ball.setMass(0.3f);
		ball.setPosition(new Vector2d(10, 5));
		ball.setVelocity(new Vector2d(5, 5).unit().mul(5));
		world.addBody(ball);
		
		int rows = 10, cols = 20;
		float width = worldWidth / cols, height = 0.2f;
		Body brick = new Body(Polygon.getRectangle(width, height));
		brick.setType(Body.DYNAMIC);
		brick.setCategories(BRICK_BIT);
		brick.setCollisionFilter(BALL_BIT | BRICK_BIT | BOUNDERY_BIT | PADDLE_BIT);
		brick.setMass(0.1f);
		
		Body tempBrick;
		
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				tempBrick = new Body(brick);
				tempBrick.setPosition(new Vector2d(col * width + width / 2, row * height + height / 2));
				world.addBody(tempBrick);
			}
		}
		
	    this.start();
	}
	
	public void start() {
		float dt = 10f;
		long startTime;
		boolean endGame = false;
		
		int[] timeSamples = new int[100];
		for (int i = 0; i < timeSamples.length; i++) timeSamples[i] = 0;
		int sum = 0;
		
		int frameTime;
		int i = 0;
		while (!endGame) {
			startTime = System.currentTimeMillis();
			renderer.clearBuffer();
			world.render();
			
			world.simulate(dt);
			renderer.renderBuffer();
			
			frameTime = (int)(System.currentTimeMillis() - startTime);
			
			sum -= timeSamples[i % timeSamples.length];
			timeSamples[i % timeSamples.length] = frameTime;
			sum += timeSamples[i % timeSamples.length];
			if (i >= timeSamples.length) System.out.println("frame time = " + (sum / timeSamples.length));
			
			//sleep the remainder of <dt> if there is any time remaining
			if (System.currentTimeMillis() < startTime + (long)dt) {
				try { Thread.sleep(startTime + (long)dt - System.currentTimeMillis()); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
			i++;
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyChar() == 'd') right = true;
		if (arg0.getKeyChar() == 'a') left = true;
		if (arg0.getKeyChar() == 'w') up = true;
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		if (arg0.getKeyChar() == 'd') right = false;
		if (arg0.getKeyChar() == 'a') left = false;
		if (arg0.getKeyChar() == 'w') up = false;
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		if (arg0.getKeyChar() == '+') zoomIn();
		if (arg0.getKeyChar() == '-') zoomOut();
		if (arg0.getKeyChar() == 'l') cameraRight();
		if (arg0.getKeyChar() == 'j') cameraLeft();
		if (arg0.getKeyChar() == 'k') cameraDown();
		if (arg0.getKeyChar() == 'i') cameraUp();
		if (arg0.getKeyChar() == 'r') resetViewport();;
	}

	@Override
	public Float preSolve(BodyPair bodyPair, IntersectionData intersectionData) {
		return 1f;
	}

	@Override
	public void postSolve(BodyPair bodyPair) {
		
	}
	
	public void zoomIn() { viewport.setPPM(viewport.getPPM() * 1.25f); }
	public void zoomOut() { viewport.setPPM(viewport.getPPM() * 0.75f); }
	public void cameraLeft() { viewport.setOrigin(viewport.getOrigin().add(new Vector2d(+viewport.getPPM(), 0))); }
	public void cameraRight() { viewport.setOrigin(viewport.getOrigin().add(new Vector2d(-viewport.getPPM(), 0))); }
	public void cameraUp() { viewport.setOrigin(viewport.getOrigin().add(new Vector2d(0, +viewport.getPPM()))); }
	public void cameraDown() { viewport.setOrigin(viewport.getOrigin().add(new Vector2d(0, -viewport.getPPM()))); }
	public void resetViewport() {
		viewport.setOrigin(new Vector2d(50, 60));
		viewport.setPPM(91);
	}
}
