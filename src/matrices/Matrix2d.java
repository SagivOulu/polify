package matrices;
/**
 * this is a mathematical matrix class containing a two dimensional matrix
 * this class is able to perform some matrix operations, such as:
 * addition, subtraction, multiplication and division.
 * @author sagiv
 *
 */
public class Matrix2d {
	private float matrix[][];
	private final int rows, cols;
	
	/**
	 * constructor of an empty matrix of a given size.
	 * all cells are set to 0
	 * @param rows number of rows in the matrix. must be grater than 0
	 * @param cols number of columns in the matrix. must be grater than 0
	 * @throws RuntimeException if one of the dimensions is not grater than 0 
	 */
	public Matrix2d(int rows, int cols) throws RuntimeException {
		if (rows < 0 || cols < 0) throw new RuntimeException("matrix must have a row and a col size of 0 or grater");
		
		this.rows = rows;
		this.cols = cols;
		
		this.matrix = new float[rows][cols];
	}
	
	/**
	 * a copy constructor for a new matrix.
	 * @param m a non null matrix instance
	 * @throws NullPointerException if the matrix parameter is null
	 */
	public Matrix2d(Matrix2d m) throws NullPointerException {
		if (m == null) throw new NullPointerException("Matrix2d constructor cannot get a null matrix instance");
		this.rows = m.getRows();
		this.cols = m.getCols();
		
		this.matrix = new float[getRows()][getCols()];
		for (int i = 0; i < getRows(); i++) {
			for (int j = 0; j < getCols(); j++) {
				set(i, j, m.get(i, j));
			}
		}
	}
	
	/**
	 * returns the number of rows in the matrix
	 * @return number of rows in the matrix
	 */
	public int getRows() { return this.rows; }
	
	/**
	 * returns the number of columns in the matrix
	 * @return number of columns in the matrix
	 */
	public int getCols() { return this.cols; }
	
	/**
	 * returns a value from a given row and column in the matrix
	 * @param row the row of the value
	 * @param col the column of the value
	 * @return the value in the given row and column
	 * @throws IndexOutOfBoundsException if the row or the column exceed the matrix size or are under 0
	 */
	public float get(int row, int col) throws IndexOutOfBoundsException {
		if (row < 0 || row >= getRows())
			throw new IndexOutOfBoundsException("row index for matrix get is out of bounds");
		if (col < 0 || col >= getCols())
			throw new IndexOutOfBoundsException("col index for matrix get is out of bounds");
		return this.matrix[row][col];
	}
	
	/**
	 * sets the value of a given cell
	 * @param row the row of the cell
	 * @param col the column of the cell
	 * @param entry the value to set the cell to
	 * @throws IndexOutOfBoundsException if the row or the column exceed the matrix size or are under 0
	 */
	public void set(int row, int col, float entry) throws IndexOutOfBoundsException {
		if (row < 0 || row >= getRows())
			throw new IndexOutOfBoundsException("row index for matrix set is out of bounds");
		if (col < 0 || col >= getCols())
			throw new IndexOutOfBoundsException("col index for matrix set is out of bounds");
		this.matrix[row][col] = entry;
	}
	
	/**
	 * multiplies the matrix by a given value, and returns the resulting matrix
	 * when multiplying a matrix by a scalar, each cell in the matrix is simply multiplied by the scalar
	 * @param scalar a value to multiply the matrix by
	 * @return the resulting matrix
	 */
	public Matrix2d mul(float scalar) {
		Matrix2d out = new Matrix2d(getRows(), getCols());
		for (int i = 0; i < getRows(); i++) {
			for (int j = 0; j < getCols(); j++) {
				out.set(i, j, get(i, j) * scalar);
			}
		}
		return out;
	}
	
	/**
	 * multiplies a matrix by another matrix. 
	 * this operation results in a new matrix.
	 * if we call this instance of the matrix i, and the parameter instance m,
	 * then i.getCols() == m.getRows() must be true in order to the operation to work.
	 * the resulting matrix has i.getRows() rows, and m.getCols() columns 
	 * @param m a matrix to multiply by
	 * @return the resulting matrix from the multiplication operation
	 * @throws RuntimeException if m is null or the matrices sizes are not to specification
	 */
	public Matrix2d mul(Matrix2d m) throws RuntimeException {
		if (m == null) throw new RuntimeException("Cannot multiply null matrices");
		if (getCols() != m.getRows()) throw new RuntimeException("In matrix multiplication m1.getCols() must be equal to m2.getRows()");
		
		Matrix2d out = new Matrix2d(getRows(), m.getCols());
		
		for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < m.getCols(); j++) {
                for (int k = 0; k < getCols(); k++) {
                    out.set(i, j, out.get(i, j) + get(i, k) * m.get(k, j));
                }
            }
        }
		
		return out;
	}
	
	/**
	 * divides a matrix by a scalar. same as multiplying a matrix by 1/scalar
	 * @param scalar a scalar to divide by. must not be 0
	 * @return the resulting matrix
	 * @throws RuntimeException if the scalar is a 0
	 */
	public Matrix2d div(float scalar) throws RuntimeException {
		if (scalar == 0) throw new RuntimeException("cannot divide a matrix by 0");
		return mul(1 / scalar);
	}
	
	/**
	 * adds two matrices together. res[i][j] = a[i][j] + b[i][j]
	 * the matrices must be of equal size
	 * @param m a matrix to add
	 * @return the resulting matrix
	 * @throws RuntimeException if the matrix is null or not the same size
	 */
	public Matrix2d add(Matrix2d m) throws RuntimeException {
		if (m == null) throw new RuntimeException("unable to add null matrices");
		if (getRows() != m.getRows() || getCols() != m.getCols())
			throw new RuntimeException("unable to add matrices of different sizes");
		Matrix2d out = new Matrix2d(getRows(), getCols());
		for (int i = 0; i < getRows(); i++)
			for (int j = 0; j < getCols(); j++)
				out.set(i, j, get(i, j) + m.get(i, j));
		
		return out;		
	}
	
	/**
	 * subtracts the given matrix from itself and returns the resulting matrix. res[i][j] = a[i][j] - b[i][j]
	 * the two matrices must be of equal size
	 * note - m1.sub(m2) is equivalent to m1.add(m2.mul(-1))
	 * @param m2 a matrix to subtract
	 * @return the resulting matrix
	 * @throws RuntimeException if the given matrix is null or not the same size
	 */
	public Matrix2d sub(Matrix2d m2) throws RuntimeException { return add(m2.mul(-1)); }
	
	/**
	 * returns a rotation matrix for the given angle in radians.
	 * @param radians the angle of rotation in radians
	 * @return a rotation matrix
	 */
	public static Matrix2d rotationMatrix2d(float radians) {
		Matrix2d out = new Matrix2d(2, 2);
		out.set(0, 0, (float)Math.cos(radians)); out.set(0, 1, -1 * (float)Math.sin(radians));
		out.set(1, 0, (float)Math.sin(radians)); out.set(1, 1, (float)Math.cos(radians));
		return out;
	}
}
