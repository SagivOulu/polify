package matrices;

/**
 * this class is a 2 dimensional vector. it allows for mathematical operations, such as:
 * vector addition, vector subtraction, dot product, scaling a vector and multiplying by a matrix
 * @author sagiv
 *
 */
public class Vector2d{
	private Matrix2d matrix;
	private float magnitude;
	private boolean magnitudeDirty = true;
	
	
	/**
	 * a constructor for a 0 vector
	 */
	public Vector2d() {
		this.matrix = new Matrix2d(2, 1);
		this.x(0);
		this.y(0);
	}
	
	/**
	 * creates a vector from a vertical two dimensional matrix of size 2x1
	 * @param matrix matrix 2x1 two dimensional matrix
	 * @throws NullPointerException if the matrix is null
	 * @throws RuntimeException if the matrix is not of size 2x1
	 */
	public Vector2d(Matrix2d matrix) throws NullPointerException, RuntimeException {
		if (matrix == null)
			throw new NullPointerException("cannot create a Vector2d from a null matrix");
		if (matrix.getRows() != 2 || matrix.getCols() != 1)
			throw new RuntimeException("a Vector2d construction matrix must be of size 2x1");
		setMatrix(matrix);
	}
	
	/**
	 * Vector2d constructor
	 * @param x x value
	 * @param y y value
	 */
	public Vector2d(float x, float y) {
		this.matrix = new Matrix2d(2, 1);
		this.x(x);
		this.y(y);
	}
	
	/**
	 * a copy constructor for a Vector2d
	 * @param v a Vector2d to copy
	 * @throws NullPointerException if the vector is null
	 */
	public Vector2d(Vector2d v) throws NullPointerException {
		if (v == null) throw new NullPointerException("Vector2d copy constructor cannot except a null Vector2d");
		setMatrix(new Matrix2d(v.getMatrix()));
	}
	
	/**
	 * constructs a vector2d, from the origin vector (from) and the destination vector(to)
	 * the resulting vector is the subtraction of the origin from the destination
	 * @param from origin point of the vector
	 * @param to destination point of the vector
	 * @throws NullPointerException if one of the given vectors is null
	 */
	public Vector2d(Vector2d from, Vector2d to) throws NullPointerException {
		if (from == null || to == null)
			throw new NullPointerException("cannot create vector from null to or from vectors");
		Vector2d v = to.sub(from);
		this.matrix = new Matrix2d(2, 1);
		this.x(v.x());
		this.y(v.y());
	}
	
	/**
	 * returns the vectors matrix representation, a 2x1 Matrix2d
	 * @return
	 */
	public Matrix2d getMatrix() { return this.matrix; }
	
	/**
	 * sets the Vectors vertical matrix to the given matrix
	 * @param matrix must be a 2x1 not null Matrix2d
	 * @throws NullPointerException if the given matrix is null
	 * @throws RuntimeException is the given matrix is not of size 2x1
	 */
	public void setMatrix(Matrix2d matrix) throws NullPointerException, RuntimeException {
		if (matrix == null) throw new NullPointerException("cannot set vector to a null matrix");
		if (matrix.getRows() != 2 || matrix.getCols() != 1)
			throw new RuntimeException("a Vector2d matrix must have 2 rows and 1 column");
		this.matrix = matrix;
		magnitudeDirty = true;
	}
	
	/**
	 * returns the vectors x value
	 * @return float
	 */
	public float x() { return getMatrix().get(0, 0); }
	/**
	 * returns the vectors y value
	 * @return float
	 */
	public float y() { return getMatrix().get(1, 0); }
	
	/**
	 * sets the vectors x value to val
	 * @param val a float float
	 */
	public void x(float val) { 
		getMatrix().set(0, 0, val);
		magnitudeDirty = true;
	}
	/**
	 * sets the vectors y value to val
	 * @param val a float float
	 */
	public void y(float val) { 
		getMatrix().set(1, 0, val);
		magnitudeDirty = true;
	}
	
	/**
	 * returns the value of the vector in the given dimension.
	 * (dimension 0 = x, dimension 1 = y)
	 * @param dimension the number of the dimension
	 * @return the value of the vector in the given dimension
	 * @throws IndexOutOfBoundsException if the dimension is lower than 0 or higher than 1
	 */
	public float get(int dimension) throws IndexOutOfBoundsException {
		if (dimension < 0 || dimension > this.getMatrix().getRows())
			throw new IndexOutOfBoundsException("vector2d dimension index is out of bounds");
		return getMatrix().get(dimension, 0);
	}
	
	/**
	 * sets the vectors value in the given dimension
	 * @param dimension the dimension of the value you want to set
	 * @param value the value to set
	 * @throws IndexOutOfBoundsException if the dimension is lower than 0 or higher than 1
	 */
	public void set(int dimension, float value) throws IndexOutOfBoundsException {
		if (dimension < 0 || dimension > this.getMatrix().getRows())
			throw new IndexOutOfBoundsException("vector2d dimension index is out of bounds");
		getMatrix().set(dimension, 0, value);
		magnitudeDirty = true;
	}
	
	private void calcMagnitude() {
		this.magnitude = (float)Math.sqrt(Math.pow(x(), 2) + Math.pow(y(), 2));
	}
	
	/**
	 * returns the magnitude of the vector. the magnitude of a vector is its length
	 * @return the magnitude of the vector
	 */
	public float magnitude() { 
		if (magnitudeDirty) calcMagnitude();
		return this.magnitude;
	}
	
	/**
	 * returns a unit vector (a vector of magnitude 1) in the same direction 
	 * @return a unit vector
	 */
	public Vector2d unit() {
		float magnitude = magnitude();
		Vector2d unitVector = new Vector2d(0, 0);
		if (magnitude != 0) {
			unitVector.x(x() / magnitude);
			unitVector.y(y() / magnitude);
		}
		return unitVector;
	}
	
	/**
	 * performs vector addition and returns the result
	 * @param v a vector to add
	 * @return the sum of the two vectors
	 * @throws NullPointerException if the given vector is null
	 */
	public Vector2d add(Vector2d v) throws NullPointerException{
		if (v == null) throw new NullPointerException("cannot add to a vector a null vector");
		return new Vector2d(getMatrix().add(v.getMatrix()));
	}
	
	/**
	 * performs vector subtraction and returns the resulting vector
	 * note - v1.sub(v2) is equivalent to v1.add(v2.mul(-1))
	 * @param v a vector to subtract
	 * @return the resulting vector
	 * @throws NullPointerException if the given vector is null
	 */
	public Vector2d sub(Vector2d v) throws NullPointerException { return add(v.mul(-1)); }
	
	/**
	 * multiplies the vector by a scalar and returns the resulting vector
	 * @param scalar float
	 * @return the resulting vector
	 */
	public Vector2d mul(float scalar) { return new Vector2d(getMatrix().mul(scalar)); }
	
	/**
	 * divides the vector by a given scalar.
	 * note - v1.div(scalar) is equivalent to v2.mul(1/scalar)
	 * @param scalar a non 0 float
	 * @return the resulting vector
	 * @throws RuntimeException if the scalar is 0
	 */
	public Vector2d div(float scalar) throws RuntimeException {
		if (scalar == 0) throw new RuntimeException("cannot divide a vector by 0");
		return mul(1 / scalar);
	}
	
	/**
	 * returns the dot product of the two vectors
	 * @param v Vector2d
	 * @return the dot product result vector
	 * @throws NullPointerException if the given vector is null
	 */
	public float dot(Vector2d v) throws NullPointerException{
		if (v == null) throw new NullPointerException("cannot proform dot product on a null vector");
		return x() * v.x() + y() * v.y();
	}
	
	public float cross2d(Vector2d v) throws NullPointerException {
		if (v == null) throw new NullPointerException("cannot perform 2d cross product on a null vector");
		return x() * v.y() - y() * v.x();
	}
	
	/**
	 * returns a rotated vector, about the origin counterclockwise by the given angle
	 * @param radians the angle to rotate in radians. positive direction is counterclockwise
	 * @return the resulting vector after the rotation
	 */
	public Vector2d rotate(float radians) { 
		return new Vector2d(Matrix2d.rotationMatrix2d(radians).mul(this.getMatrix())); 
	}
	
	@Override
	public String toString() {
		return "<" + this.x() + ", " + this.y() + ">";
	}
	
	public String toRoundedString(int decimalPoints) {
		float pow = (float)Math.pow(10, decimalPoints);
		return "<" + 
				(float)((int)(this.x()*pow))/pow + 
				", " + 
				(float)((int)(this.y()*pow))/pow + 
				">";
	}
	
	public boolean isEqual(Vector2d v) throws NullPointerException {
		if (v == null) throw new NullPointerException("cannot compare a vector to a null vector");
		return this.x() == v.x() && this.y() == v.y();
	}
}